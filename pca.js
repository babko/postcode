(function ($) {
  // Store our function as a property of Drupal.behaviors.
  Drupal.behaviors.postCodeAnywhere = {
    attach: function (context, settings) {
      //var FieldsWrapper = $(Drupal.settings.postcodeanywhere.id_postcode).parent().parent();
      $('#payment-details fieldset .panel-body :input', context).autocomplete({
        source: Drupal.settings.basePath + 'postcodeanywhere/findbyterm',
        select: function( event, ui ) {
        event.preventDefault();
          
        $('#payment-details fieldset .panel-body :input', context).prop("disabled", true);
        $(this).val(ui.item.label);
//        $(this).wrap('<div class="input-group"></div>');
//        $(this).after('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>')
        $.getJSON(Drupal.settings.basePath+"postcodeanywhere/retrievebyid/",{parid:ui.item.value}, function(data){
                // Set the Values
              //window.console.log(data);
              if(typeof(data['error']) === 'undefined'){
                var id_company = $(Drupal.settings.postcodeanywhere.id_company);
                var id_line1 = $(Drupal.settings.postcodeanywhere.id_line1);
                var id_line2 = $(Drupal.settings.postcodeanywhere.id_line2);
                var id_town = $(Drupal.settings.postcodeanywhere.id_town);
                var id_postcode = $(Drupal.settings.postcodeanywhere.id_postcode);
                if (id_company.length > 1) $(id_company[0]).val(data[0].Company[0]).change(); else id_company.val(data[0].Company[0]).change();
                if (id_line1.length > 1) $(id_line1[0]).val(data[0].Line1[0]).change(); else id_line1.val(data[0].Line1[0]).change();
                if (id_line2.length > 1) $(id_line2[0]).val(data[0].Line2[0]).change(); else id_line2.val(data[0].Line2[0]).change();
                if (id_town.length > 1) $(id_town[0]).val(data[0].City[0]).change(); else id_town.val(data[0].City[0]).change();
                if (id_postcode.length > 1) $(id_postcode[0]).val(data[0].PostalCode[0]).change(); else id_postcode.val(data[0].PostalCode[0]).change();
          
        // Show the Wrappers.
          $(Drupal.settings.postcodeanywhere.id_company_wrapper).show();
          $(Drupal.settings.postcodeanywhere.id_line1_wrapper).show();
          $(Drupal.settings.postcodeanywhere.id_line2_wrapper).show();
          $(Drupal.settings.postcodeanywhere.id_town_wrapper).show();
          $(Drupal.settings.postcodeanywhere.id_county_wrapper).show();
              }
              $('#payment-details fieldset .panel-body :input', context).prop("disabled", false);
              return false;
        });
        },
        focus: function( event, ui ) {
          $(this).val(ui.item.label);
          return false;
          }
        });
      }
    }
}(jQuery));

